import CheckBox from '@react-native-community/checkbox';
import {Dropdown} from 'react-native-material-dropdown-v2';
import React, {useRef, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {width} from '../styles/globalStyle';
import {MainView} from './Components/Container';
import {Button} from './Components/CustomButton';
import {Header} from './Components/CustomHeader';
import {Input} from './Components/CustomInput';
import DateTime from './Components/DateTime';
import moment from 'moment';
import {useStore} from 'react-redux';
const CreateAccount = ({navigation}) => {
  const {CountryPhoneCode} = useStore().getState();
  const [state, setState] = useState({
    username: {
      value: 'ottest1@optiontown.com',
      error: false,
      errorMessage: '',
    },
    confirmusername: {
      value: 'ottest1@optiontown.com',
      error: false,
      errorMessage: '',
    },
    password: {
      value: 'tngsoft',
      error: false,
      errorMessage: '',
    },
    confirmpassword: {
      value: 'tngsoft',
      error: false,
      errorMessage: '',
    },
    firstname: {
      value: '',
      error: false,
      errorMessage: '',
    },
    middlename: {
      value: '',
      error: false,
      errorMessage: '',
    },
    lastname: {
      value: '',
      error: false,
      errorMessage: '',
    },
    mobile: {
      value: '',
      error: false,
      errorMessage: '',
    },
    email: '',
    dob: '',
    date: '',
    showDatePicker: false,
  });
  const genderRef = useRef();
  const prefixRef = useRef();
  const isdRef = useRef();
  const onChangeDobHandler = ({type, nativeEvent}) => {
    if (type === 'set') {
      setState({
        ...state,
        showDatePicker: false,
        dob: moment(nativeEvent.timestamp).format('YYYY-MM-DD'),
        date: nativeEvent.timestamp,
      });
    } else {
      setState({...state, showDatePicker: false});
    }
  };
  const onNavigateLogin = () => {
    navigation.replace('Login');
  };
  const onSubmitHandler = () => {
    consolejson(CountryPhoneCode);
  };
  return (
    <MainView>
      <Header title={'Create Account'} onPress={navigation.goBack} />
      <ScrollView>
        <Text style={styles.textMandatory}>{`(All fields are mandatory)`}</Text>
        <Input
          label={'Username / Email'}
          value={state.username.value}
          error={state.username.error}
          errorMessage={state.username.errorMessage}
          onChangeText={value =>
            setState({
              ...state,
              username: {...state.username, value, error: false},
            })
          }
        />
        <Input
          label={'Confirm Email Address'}
          value={state.confirmusername.value}
          error={state.confirmusername.error}
          errorMessage={state.confirmusername.errorMessage}
          onChangeText={value =>
            setState({
              ...state,
              confirmusername: {...state.confirmusername, value, error: false},
            })
          }
        />
        <Input
          label={'Password'}
          value={state.password.value}
          error={state.password.error}
          errorMessage={state.password.errorMessage}
          secureTextEntry={true}
          onChangeText={value =>
            setState({
              ...state,
              password: {...state.password, value, error: false},
            })
          }
        />

        <Input
          label={'Confirm Password'}
          value={state.confirmpassword.value}
          error={state.confirmpassword.error}
          errorMessage={state.confirmpassword.errorMessage}
          secureTextEntry={true}
          onChangeText={value =>
            setState({
              ...state,
              confirmpassword: {...state.confirmpassword, value, error: false},
            })
          }
        />
        <Text
          style={
            styles.textPasswordHint
          }>{`(8-20 size. a-z, 0-9 and special characters)`}</Text>

        <Text style={styles.textPeronalInfo}>{`Personal Information`}</Text>
        {/* <Text style={styles.textPrefix}>{`Prefix`}</Text> */}

        <Dropdown
          ref={prefixRef}
          rippleOpacity={0.54}
          dropdownPosition={-2}
          onChangeText={prefix => setState({...state, prefix})}
          data={prefixList}
          labelExtractor={item => item.value}
          valueExtractor={item => item.value}
          containerStyle={{height: 0, width: 0}}
          pickerStyle={{width: width - 40, left: 20}}
        />
        <TouchableOpacity onPress={() => prefixRef.current.focus()}>
          <Input
            label={'Prefix'}
            editable={false}
            value={state.prefix}
            error={false}
            errorMessage={''}
          />
        </TouchableOpacity>

        <Input
          label={'First Name'}
          value={state.firstname.value}
          error={state.firstname.error}
          errorMessage={state.firstname.errorMessage}
          onChangeText={value =>
            setState({
              ...state,
              firstname: {...state.firstname, value, error: false},
            })
          }
        />
        <Input
          label={'Middle Name'}
          value={state.middlename.value}
          error={state.middlename.error}
          errorMessage={state.middlename.errorMessage}
          onChangeText={value =>
            setState({
              ...state,
              middlename: {...state.middlename, value, error: false},
            })
          }
        />

        <Input
          label={'Last Name'}
          value={state.lastname.value}
          error={state.lastname.error}
          errorMessage={state.lastname.errorMessage}
          onChangeText={value =>
            setState({
              ...state,
              lastname: {...state.lastname, value, error: false},
            })
          }
        />

        <TouchableOpacity
          onPress={() => setState({...state, showDatePicker: true})}>
          <Input
            label={'Date of Birth'}
            editable={false}
            value={state.dob}
            error={false}
            errorMessage={''}
          />
        </TouchableOpacity>

        <Dropdown
          ref={genderRef}
          rippleOpacity={0.54}
          dropdownPosition={-2}
          onChangeText={gender => setState({...state, gender})}
          data={genderList}
          labelExtractor={item => item.value}
          valueExtractor={item => item.value}
          containerStyle={{height: 0, width: 0}}
          pickerStyle={{width: width - 40, left: 20}}
        />
        <TouchableOpacity onPress={() => genderRef.current.focus()}>
          <Input
            label={'Gender'}
            editable={false}
            value={state.gender}
            error={false}
            errorMessage={''}
          />
        </TouchableOpacity>
        <Dropdown
          ref={isdRef}
          rippleOpacity={0.54}
          dropdownPosition={-2}
          onChangeText={isd => setState({...state, isd})}
          data={CountryPhoneCode}
          labelExtractor={item => item.name}
          valueExtractor={item => item.name}
          containerStyle={{height: 0, width: 0}}
          pickerStyle={{width: width - 40, left: 20}}
        />

        <TouchableOpacity onPress={() => isdRef.current.focus()}>
          <Input
            label={'ISD'}
            editable={false}
            value={state.isd}
            error={false}
            errorMessage={''}
          />
        </TouchableOpacity>
        <Input
          label={'Cell/Mobile'}
          value={state.mobile.value}
          error={state.mobile.error}
          errorMessage={state.mobile.errorMessage}
          keyboardType={'phone-pad'}
          onChangeText={value =>
            setState({
              ...state,
              mobile: {...state.mobile, value, error: false},
            })
          }
        />

        <View style={styles.viewCheckBox}>
          <CheckBox
            value={state.check}
            tintColors={{
              true: '#EF4236',
              false: 'grey',
            }}
            onValueChange={check => setState({...state, check})}
          />
          <Text>{` Please accept our `}</Text>
          <TouchableOpacity>
            <Text style={styles.textTermCondition}>{`Terms of Service`}</Text>
          </TouchableOpacity>
        </View>

        <Button
          title={'Submit'}
          containerStyle={{marginHorizontal: 20}}
          onPress={onSubmitHandler}
        />
        <TouchableOpacity style={styles.touchLogin} onPress={onNavigateLogin}>
          <Text style={styles.textLogin}>{`Already a member? `}</Text>
          <Text style={styles.textLogin2}>{`Login`}</Text>
          <Text></Text>
        </TouchableOpacity>
        <View style={styles.viewFooter}>
          <Text>{`By clicking on "Submit", you are agreeing to the `}</Text>
          <Text>{`Terms of Services `}</Text>
          <Text>{`and `}</Text>
          <Text>{`Privacy Policy`}</Text>
        </View>
      </ScrollView>
      <DateTime
        value={state.date}
        mode={'date'}
        visible={state.showDatePicker}
        onChange={onChangeDobHandler}
      />
    </MainView>
  );
};
export default CreateAccount;

const styles = StyleSheet.create({
  viewCheckBox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
    marginTop: 10,
    marginBottom: 15,
  },
  textTermCondition: {
    color: '#0039AC',
  },
  touchLogin: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    marginRight: 20,
    padding: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#6894c4',
    marginTop: 20,
  },
  textLogin: {},
  textLogin2: {
    color: '#0039ac',
  },
  textMandatory: {
    color: '#333333',
    fontSize: 11,
    alignSelf: 'center',
    marginVertical: 15,
  },
  textPasswordHint: {
    color: '#333333',
    fontSize: 11,
    marginHorizontal: 30,
    marginBottom: 15,
  },
  textPeronalInfo: {
    marginHorizontal: 20,
    color: '#333333',
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 15,
    marginTop: 5,
  },
  textPrefix: {
    marginHorizontal: 20,
    marginTop: 20,
    color: '#333333',
    fontSize: 16,
    fontWeight: 'bold',
  },
  viewFooter: {
    margin: 20,
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'center',
  },
});

const genderList = [
  {
    id: '1',
    value: 'Male',
  },
  {
    id: '2',
    value: 'Female',
  },
];
const prefixList = [
  {
    id: '1',
    value: 'Mr.',
  },
  {
    id: '2',
    value: 'Mrs.',
  },
  {
    id: '3',
    value: 'Miss',
  },
  {
    id: '4',
    value: 'Ms.',
  },
  {
    id: '4',
    value: 'Others',
  },
];
