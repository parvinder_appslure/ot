import React from 'react';
import {StyleSheet} from 'react-native';
import {MainView} from '../Components/Container';
import Flight from './Flight';
import Header from './Header';

const Home = () => {
  return (
    <MainView>
      <Header />
      <Flight />
    </MainView>
  );
};

const styles = StyleSheet.create({});
export default Home;
