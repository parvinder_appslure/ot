import React, {memo} from 'react';
import {
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSelector} from 'react-redux';
import {width} from '../../styles/globalStyle';
const Flight = memo(() => {
  const {StaticBanner, Label} = useSelector(store => store);
  return (
    <ScrollView contentContainerStyle={styles.container}>
      {StaticBanner.map(item => {
        const {Button_Icon, Product_ID, Short_Label} = item;
        return (
          <Options
            key={Product_ID}
            title={Short_Label}
            source={{uri: Button_Icon}}
          />
        );
      })}
    </ScrollView>
  );
});

const Options = memo(({title, source}) => {
  return (
    <TouchableOpacity style={styles.op_touch}>
      <ImageBackground source={source} style={styles.op_image}>
        <View style={styles.op_viewTitle}>
          <Text style={styles.op_textTitle}>{title}</Text>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
});
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 8,
    flexWrap: 'wrap',
  },
  op_touch: {
    margin: 8,
  },
  op_image: {
    width: width / 2 - 24,
    height: width / 2 - 24,
    flexDirection: 'column-reverse',
  },
  op_viewTitle: {
    backgroundColor: '#000000cc',
    padding: 5,
    paddingHorizontal: 10,
  },
  op_textTitle: {
    fontSize: 14,
    fontWeight: '500',
    color: '#fff',
  },
});
export default Flight;
