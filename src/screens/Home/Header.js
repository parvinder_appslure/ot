import {useNavigation} from '@react-navigation/core';
import React, {memo} from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';

const Header = memo(() => {
  const navigation = useNavigation();
  const onProfileHandler = () => {
    navigation.navigate('Login');
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.touchMenu}
        onPress={navigation.openDrawer}>
        <Image
          source={require('../../assets/menu.png')}
          style={styles.imageProfile}
        />
      </TouchableOpacity>
      <Image
        source={require('../../assets/logo.png')}
        style={styles.imageLogo}
      />
      <TouchableOpacity style={styles.touchProfile} onPress={onProfileHandler}>
        <Image
          source={require('../../assets/Login_Icon.png')}
          style={styles.imageProfile}
        />
      </TouchableOpacity>
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 10,
    backgroundColor: 'white',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  touchMenu: {
    padding: 5,
  },
  touchProfile: {
    marginLeft: 'auto',
    padding: 5,
  },
  imageProfile: {
    width: 30,
    height: 24,
    resizeMode: 'contain',
  },
  imageLogo: {
    width: 150,
    height: 30,
    resizeMode: 'contain',
    marginHorizontal: 5,
  },
});

export default Header;
