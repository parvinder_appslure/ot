import React, {memo} from 'react';
import {View} from 'react-native';
import {TextInput, HelperText} from 'react-native-paper';

export const Input = memo(props => {
  return (
    <View style={[{marginVertical: 0, marginHorizontal: 30}, props?.viewStyle]}>
      <TextInput mode={'flat'} {...props} />
      <HelperText type="error" visible={props.error} style={{marginTop: 2}}>
        {props.errorMessage}
      </HelperText>
    </View>
  );
});
