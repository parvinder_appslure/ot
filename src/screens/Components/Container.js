import React from 'react';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarDark, StatusBarLight} from './CustomStatusBar';

export const MainView = props => (
  <SafeAreaView style={[styles.container, props?.containerStyle]}>
    {props.children}
  </SafeAreaView>
);

export const MainViewLight = props => (
  <SafeAreaView style={styles.container}>
    <StatusBarLight />
    {props.children}
  </SafeAreaView>
);
export const MainViewDark = props => (
  <SafeAreaView style={styles.container}>
    <StatusBarDark />
    {props.children}
  </SafeAreaView>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  viewUnderline: {
    borderRadius: 3,
    borderColor: '#4FA980',
    marginHorizontal: 30,
    borderWidth: 3,
    width: '10%',
    marginVertical: 10,
  },
  textTitle: {
    fontSize: 28,
    fontWeight: 'bold',
    fontFamily: 'Avenir-Heavy',
    color: '#100C08',
    marginTop: 20,
    marginHorizontal: 30,
    lineHeight: 40,
  },
  textSubTitle: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#100C08',
    opacity: 0.5,
    lineHeight: 22,
    marginHorizontal: 30,
    marginTop: 10,
  },
  textError: {
    fontFamily: 'Avenir-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#100C08',
  },
});
