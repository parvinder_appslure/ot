import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

export const Header = props => (
  <View style={styles.container}>
    <TouchableOpacity style={styles.touchBack} onPress={props.onPress}>
      <Image
        source={require('../../assets/leftArrow.png')}
        style={styles.imageBack}
      />
    </TouchableOpacity>
    <Image
      source={require('../../assets/OT_Icon.png')}
      style={styles.imageIcon}
    />
    <Text style={styles.textTitle}>{props.title}</Text>
    {props.children}
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
    backgroundColor: '#ffffff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  touchBack: {
    padding: 5,
  },
  imageBack: {
    width: 20,
    resizeMode: 'contain',
  },
  textTitle: {
    flex: 1,
    fontWeight: '500',
    fontSize: 20,
    color: '#000',
  },
  imageIcon: {
    width: 30,
    height: 30,
    marginHorizontal: 10,
    resizeMode: 'contain',
  },
});
