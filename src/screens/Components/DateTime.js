import React from 'react';

import DateTimePicker from '@react-native-community/datetimepicker';
const DateTime = ({value, visible = false, mode, onChange}) => {
  return (
    visible && (
      <DateTimePicker
        testID="dateTimePicker"
        minimumDate={new Date()}
        value={value || new Date()}
        mode={mode}
        // is24Hour={true}
        display="spinner"
        onChange={onChange}
      />
    )
  );
};
export default DateTime;
