import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';

export const Button = ({
  title,
  onPress,
  containerStyle = {},
  textStyle = {},
}) => (
  <TouchableOpacity
    style={[styles.touchButton, containerStyle]}
    onPress={onPress}>
    <Text style={[styles.textButton, textStyle]}>{title}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  touchButton: {
    backgroundColor: '#CC3300',
    borderRadius: 5,
    paddingVertical: 8,
    alignItems: 'center',
  },
  textButton: {
    fontWeight: 'bold',
    color: '#fff',
  },
});
