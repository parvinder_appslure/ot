import React from 'react';
import {StatusBar} from 'react-native';

export const StatusBarLight = props => (
  <StatusBar
    translucent={false}
    hidden={false}
    backgroundColor="#4FA980"
    barStyle={'light-content'}
    {...props}
  />
);

export const StatusBarDark = props => (
  <StatusBar
    translucent={false}
    hidden={false}
    backgroundColor="#4FA980"
    barStyle={'dark-content'}
    {...props}
  />
);
