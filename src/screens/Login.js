import React, {memo, useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSelector} from 'react-redux'; //TODO: to add label object inside component
import {Api} from '../service/api/Api';
import {MainView} from './Components/Container';
import {Button} from './Components/CustomButton';
import {Header} from './Components/CustomHeader';
import {Input} from './Components/CustomInput';
const Login = ({navigation}) => {
  const {Label} = useSelector(store => store); //TODO: to add label object inside component
  const [state, setState] = useState({
    username: {
      value: 'ottest1@optiontown.com',
      error: false,
      errorMessage: '',
    },
    password: {
      value: 'tngsoft',
      error: false,
      errorMessage: '',
    },
  });
  const onLoginHandler = async () => {
    const {username, password} = state;
    // console.log('login api');
    if (username.value === '') {
      username.errorMessage = 'Username is required';
      username.error = true;
      setState({...state, username: {...state.username, ...username}});
      return;
    }
    if (password.value === '') {
      password.errorMessage = 'Password is required';
      password.error = true;
      setState({...state, password: {...state.password, ...password}});
      return;
    }

    const response = await Api.login({
      'customer.userName': username.value,
      'customer.passwd': password.value,
      isFromMyaccountPage: '1',
      countryId: '99',
      languageId: '1',
    });
    consolejson(response);
    const {Result = '', CompleteProfile = {}} = response;
    if (Result === 'Success') {
    }
  };
  return (
    <MainView>
      <Header title={Label?.LABL_Login_Label} onPress={navigation.goBack} />
      <ScrollView>
        <Text style={styles.textTitle}>
          {Label?.LABL_MA_Login_Page_Heading_Label}
        </Text>
        <Input
          label={Label?.LABL_Username_Label}
          value={state.username.value}
          error={state.username.error}
          errorMessage={state.username.errorMessage}
          onChangeText={value =>
            setState({
              ...state,
              username: {...state.username, value, error: false},
            })
          }
        />
        <Input
          label={Label?.LABL_Password_Label}
          value={state.password.value}
          error={state.password.error}
          errorMessage={state.password.errorMessage}
          secureTextEntry={true}
          onChangeText={value =>
            setState({
              ...state,
              password: {...state.password, value, error: false},
            })
          }
        />
        <TouchableOpacity style={styles.touchForgot}>
          <Text style={styles.textForgot}>
            {Label?.LABL_Forgot_Password_Label}
          </Text>
        </TouchableOpacity>
        <Button
          title={Label?.LABL_Login_Label}
          containerStyle={{margin: 20}}
          onPress={onLoginHandler}
        />
        <View style={styles.viewSignup}>
          <Text>{Label?.LABL_MA_Login_Join_Heading_Label}</Text>
          <ListView title={Label?.LABL_MA_Login_Join_Benefit1_Message} />
          <ListView title={Label?.LABL_MA_Login_Join_Benefit2_Message} />
          <ListView title={Label?.LABL_MA_Login_Join_Benefit3_Message} />
          <Button
            title={Label?.LABL_Join_Optiontown_Label}
            containerStyle={{marginVertical: 5}}
          />
        </View>
      </ScrollView>
    </MainView>
  );
};
const styles = StyleSheet.create({
  textTitle: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#000',
    margin: 20,
  },
  touchForgot: {
    margin: 20,
  },
  textForgot: {
    fontSize: 16,
    fontWeight: '500',
    color: '#0039AC',
  },
  viewSignup: {
    margin: 20,
    marginTop: 0,
    padding: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#6894C4',
  },
});

const ListView = memo(({title}) => (
  <View style={{flexDirection: 'row', margin: 5}}>
    <Text>{'\u2022 '}</Text>
    <Text>{title}</Text>
  </View>
));
export default Login;
