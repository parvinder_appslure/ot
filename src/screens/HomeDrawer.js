import React, {memo, useEffect, useCallback, useState} from 'react';
import {
  StyleSheet,
  Image,
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import * as actions from '../service/redux/actions';
const HomeDrawer = ({navigation}) => {
  const {CountrySelected} = useSelector(store => store);
  const dispatch = useDispatch();
  const [state, setState] = useState({
    showFlight: true,
    showOptionPass: false,
    showHelp: false,
    showCountry: false,
    CountrySelected: CountrySelected || {},
  });
  const onShowCountryList = () => {
    setState({...state, showCountry: true});
  };
  const onSelectCountryHandler = ({type, CountrySelected}) => {
    if (type === 'close') {
      setState({...state, showCountry: false});
    }
    if (type === 'select') {
      setState({...state, CountrySelected, showCountry: false});

      dispatch(actions.CountrySelected(CountrySelected));
    }
  };
  const onCreateAccountHandler = useCallback(() => {
    navigation.navigate('CreateAccount');
  }, []);
  const onLoginHandler = useCallback(() => {
    navigation.navigate('Login');
  }, []);
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <View style={styles.viewTop}>
          <Button
            title={'Create My Account'}
            onPress={onCreateAccountHandler}
          />
          <Button title={'Login'} onPress={onLoginHandler} />
        </View>
        <TouchableOpacity
          style={styles.touchCountry}
          onPress={onShowCountryList}>
          <Text style={styles.textBold}>Change Country</Text>
          <Image
            style={styles.imageCountry}
            source={
              state.CountrySelected?.CountryLogo
                ? {uri: state.CountrySelected?.CountryLogo}
                : require('../assets/menu.png')
            }
          />
          <Text style={styles.textBold}>
            {state.CountrySelected?.CountryName}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.touchCountry}>
          <Text style={styles.textBold}>Buy Flight Options</Text>
          <Text style={styles.textExpand}>{state.showFlight ? '-' : '+'}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.touchCountry}>
          <Text style={styles.textBold}>Buy Option Pass</Text>
          <Text style={styles.textExpand}>
            {state.showOptionPass ? '-' : '+'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.touchCountry}>
          <Text style={styles.textBold}>Help</Text>
          <Text style={styles.textExpand}>{state.showHelp ? '-' : '+'}</Text>
        </TouchableOpacity>
      </ScrollView>
      <SelectCountry
        visible={state.showCountry}
        onPress={onSelectCountryHandler}
        CountrySelected={state.CountrySelected}
      />
    </SafeAreaView>
  );
};
const Button = memo(({title, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.touchButton}>
      <Text style={styles.textButton}>{title}</Text>
    </TouchableOpacity>
  );
});
const SelectCountry = memo(
  ({visible = false, CountrySelected = {}, onPress}) => {
    const {CountryList} = useSelector(store => store);
    const onCountrySelectHandler = CountrySelected => {
      onPress({type: 'select', CountrySelected});
    };
    consolejson(CountrySelected?.CountryID);
    return (
      visible && (
        <View style={styles.sc_container}>
          <View style={styles.sc_viewTop}>
            <TouchableOpacity
              onPress={() => {
                onPress({type: 'close'});
              }}>
              <Image
                style={styles.sc_imageBack}
                source={require('../assets/leftArrow.png')}
              />
            </TouchableOpacity>
            <Text style={styles.sc_textTitle}>Select a Country</Text>
          </View>
          <ScrollView>
            {CountryList.map((item, index) => {
              const {CountryID, CountryLogo, CountryName, Language} = item;
              const bolCheck = CountrySelected?.CountryID === CountryID;
              return (
                <TouchableOpacity
                  key={`key_${index}`}
                  style={styles.sc_touch}
                  onPress={() => onCountrySelectHandler(item)}>
                  <Image
                    source={{uri: CountryLogo}}
                    style={styles.sc_imageCountry}
                  />
                  <Text
                    style={
                      styles.sc_textCountryLang
                    }>{`${CountryName}-${Language}`}</Text>
                  {bolCheck && (
                    <Image
                      source={require('../assets/checkbox-mark.png')}
                      style={styles.sc_imageCheck}
                    />
                  )}
                </TouchableOpacity>
              );
            })}
          </ScrollView>
        </View>
      )
    );
  },
);
const styles = StyleSheet.create({
  viewTop: {
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 10,
    justifyContent: 'space-evenly',
  },
  touchButton: {
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  textButton: {},
  textExpand: {
    marginLeft: 'auto',
    marginRight: 5,
    color: '#5e5e5e',
    fontSize: 18,
  },
  touchCountry: {
    flexDirection: 'row',
    backgroundColor: '#f7f7f7',
    paddingVertical: 12,
    paddingHorizontal: 16,
    marginVertical: 10,
    alignItems: 'center',
  },
  textBold: {
    fontFamily: 'arial',
  },
  imageCountry: {
    width: 30,
    height: 20,
    resizeMode: 'contain',
    marginLeft: 'auto',
    marginRight: 10,
  },
  sc_container: {
    position: 'absolute',
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
    elevation: 2,
  },
  sc_viewTop: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#00000080',
    borderBottomWidth: 1,
    padding: 10,
  },
  sc_imageBack: {
    width: 15,
    resizeMode: 'contain',
  },
  sc_textTitle: {
    marginHorizontal: 10,
  },
  sc_imageCountry: {
    width: 30,
    height: 20,
    resizeMode: 'contain',
  },
  sc_textCountryLang: {
    marginHorizontal: 5,
  },
  sc_touch: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    paddingVertical: 20,
    borderBottomColor: '#0000001a',
    borderBottomWidth: 1,
  },
  sc_imageCheck: {
    marginLeft: 'auto',
    marginRight: 10,
    width: 16,
    resizeMode: 'contain',
  },
});
export default HomeDrawer;
