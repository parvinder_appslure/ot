import React, {useEffect} from 'react';
import {Text, View} from 'react-native';
import {useDispatch} from 'react-redux';
import * as actions from '../service/redux/actions';
import {Api} from '../service/api/Api';
const Splash = ({navigation}) => {
  const dispatch = useDispatch();
  const fetchStaticBanner = new Promise(async (resolve, reject) => {
    const response = await Api.home({
      CountryId: '99',
      tgProductId: '9',
      LanguageId: '1',
    });
    const {
      status = false,
      msg,
      data: {StaticBanner = []},
    } = response;
    if (status) {
      // consolejson(StaticBanner);
      dispatch(actions.StaticBanner(StaticBanner));
      resolve(true);
    } else {
      reject(false);
    }
  });
  const fetchLabel = new Promise(async (resolve, reject) => {
    const response = await Api.label({
      CountryId: '99',
      TgProductId: '9',
      AirlineId: '0',
      LanguageId: '1',
    });
    const {status = false, msg, data = []} = response;
    if (status) {
      dispatch(actions.Label(data));
      resolve(true);
    } else {
      resolve(false);
    }
  });
  const fetchCountryList = new Promise(async (resolve, reject) => {
    const response = await Api.countryList({
      CountryId: '99',
      tgProductId: '9',
      isFromMyaccountPage: '1',
      LanguageId: '1',
    });
    const {
      status = false,
      msg,
      data: {CountryList = [], CountryPhoneCode = []},
    } = response;
    if (status) {
      dispatch(actions.CountryList({CountryList, CountryPhoneCode}));
      resolve(true);
    } else {
      reject(false);
    }
  });
  useEffect(() => {
    Promise.all([fetchStaticBanner, fetchCountryList, fetchLabel])
      .then(values => {
        // consolejson(values);
        if (!values.includes(false)) {
          navigation.replace('DrawerNavigator');
        } else {
          console.log('Splash screen promise fail');
        }
      })
      .catch(error => {
        console.log({error});
      });
  }, []);

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text>Option town splash screen</Text>
    </View>
  );
};
export default Splash;
