import React from 'react';
import {Provider} from 'react-redux';
import store from './service/redux/store';
// import * as actions from './service/redux/actions';
import StackNavigator from './navigator/StackNavigator';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
window.consolejson = json => console.log(JSON.stringify(json, null, 2));

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#ACB1C0',
    accent: '#f1c40f',
    error: '#CC3300',
  },
};
const App = () => {
  return (
    <Provider store={store}>
      <PaperProvider theme={theme}>
        <StackNavigator></StackNavigator>
      </PaperProvider>
    </Provider>
  );
};

export default App;
