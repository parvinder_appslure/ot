// import AsyncStorage from '@react-native-async-storage/async-storage';
import {_post} from './ApiSauce';
// import {Dimensions} from 'react-native';
// const {width, height} = Dimensions.get('window');

// const AspectRatio = () => width / height;

export const Api = {
  home: json =>
    _post('getSellerList.do?mobileAction=getStaticBannerList', json),
  countryList: json =>
    _post('getSellerList.do?mobileAction=getCountryList', json),
  login: json => _post('getSellerList.do?mobileAction=setLoginDetails', json),
  createAccount: json =>
    _post('getSellerList.do?mobileAction=CreateAccount', json),
  label: json => _post('getSellerList.do?mobileAction=ApiLabels', json),
};
// const LocalStorage = {
//   setToken: token => AsyncStorage.setItem('authToken', token),
//   getToken: () => AsyncStorage.getItem('authToken'),
//   setFirstTimeOpen: () => AsyncStorage.setItem('firstTimeOpen', 'false'),
//   getFirstTimeOpen: () => AsyncStorage.getItem('firstTimeOpen'),
//   clear: AsyncStorage.clear,
// };
