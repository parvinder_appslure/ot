import {create} from 'apisauce';
import {BASE_URL} from '../../Config';

const ApiSauceJson = {
  baseURL: BASE_URL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
};
const ApiSauceJsonMulitpart = {
  baseURL: BASE_URL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'multipart/form-data',
  },
};

const ApiSauce = create(ApiSauceJson);
const ApiSauceMultiPart = create(ApiSauceJsonMulitpart);

export const _post = (path, json) => {
  return new Promise((resolve, reject) => {
    ApiSauce.post(path, json).then(response => {
      if (response.ok) {
        resolve(response.data);
      } else {
        console.log(response.err);
        reject(response.err);
      }
    });
  });
};
export const _postMultipart = (path, form) => {
  return new Promise((resolve, reject) => {
    ApiSauceMultiPart.post(path, form).then(response => {
      if (response.ok) {
        resolve(response.data);
      } else {
        reject(response);
      }
    });
  });
};

// export const _SetAuthToken = token => {
//   ApiSauce.setHeader('Authorization', token);
//   ApiSauceMultiPart.setHeader('Authorization', token);
// };
// export const _RemoveAuthToken = () => {
//   ApiSauce.deleteHeader('Authorization');
//   ApiSauceMultiPart.deleteHeader('Authorization');
// };
