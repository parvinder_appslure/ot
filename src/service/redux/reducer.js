import {act} from 'react-test-renderer';

const data = {
  StaticBanner: [],
  CountryList: [],
  CountrySelected: {},
  CountryPhoneCode: [],
  Label: [],
};
const reducer = (state = data, action) => {
  console.log(':::::::: action::', action.type);
  if (action.type === 'CountrySelected') {
    consolejson(action.payload);
  }
  switch (action.type) {
    case 'StaticBanner':
      return {
        ...state,
        StaticBanner: action.payload,
      };
    case 'CountryList':
      return {
        ...state,
        CountryList: action.payload.CountryList,
        CountryPhoneCode: action.payload.CountryPhoneCode,
      };
    case 'CountrySelected':
      return {
        ...state,
        CountrySelected: action.payload.CountrySelected,
      };
    case 'Label':
      return {
        ...state,
        Label: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
