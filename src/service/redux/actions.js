export const StaticBanner = payload => ({type: 'StaticBanner', payload});
export const CountryList = payload => ({type: 'CountryList', payload});
export const CountrySelected = payload => ({type: 'CountrySelected', payload});
export const Label = payload => ({type: 'Label', payload});
