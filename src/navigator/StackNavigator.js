import React, {useRef, useEffect} from 'react';
import {CardStyleInterpolators} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Splash from '../screens/Splash';
import Login from '../screens/Login';
import DrawerNavigator from './DrawerNavigator';
import CreateAccount from '../screens/CreateAccount';

const Stack = createNativeStackNavigator();
const StackNavigator = () => {
  const navigationRef = useRef();
  // useReduxDevToolsExtension(navigationRef);

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => console.log(navigationRef.current.getCurrentRoute().name)}
      onStateChange={() =>
        console.log(`Screen :: `, navigationRef.current.getCurrentRoute().name)
      }>
      <Stack.Navigator
        initialRouteName={'Splash'}
        screenOptions={{
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="DrawerNavigator"
          component={DrawerNavigator}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="CreateAccount"
          component={CreateAccount}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default StackNavigator;
