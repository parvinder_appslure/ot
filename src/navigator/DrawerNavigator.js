import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import HomeDrawer from '../screens/HomeDrawer';
import Home from '../screens/Home';

const Drawer = createDrawerNavigator();
const DrawerNavigator = () => (
  <Drawer.Navigator
    initialRouteName="Home"
    drawerStyle={{width: '70%'}}
    drawerContent={props => <HomeDrawer {...props} />}>
    <Drawer.Screen
      name="Home"
      component={Home}
      options={{headerShown: false}}
    />
  </Drawer.Navigator>
);
export default DrawerNavigator;
